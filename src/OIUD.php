<?php
namespace Uxin603s;
trait OIUD{
	public static function process_field($array){
		$field_array=[];
		if(isset($array['fields'])){	
			foreach($array['fields'] as $field){
				if(in_array($field,static::$field_array)){
					$field_array[]=$field;
				}
			}		
		}
		if(isset($array['custom']['fields'])){
			foreach($array['custom']['fields'] as $field){
				$field_array[]=$field;
			}
		}
		if($field_array){
			$fields=implode(",",$field_array);
		}else{
			$fields="*";
		}
		return $fields;
	}
	
	public static function process_where($array,&$bind_data){
		$operator_list=["=",">","<","<=",">=","!=",'like'];
		$and_where_array=[];
		$sql="";
		if(isset($array['where'])){		
			$field_array=[];
			foreach($array['where'] as $item){		
				if(!in_array($item['field'],static::$field_array)){
					continue;
				}
				if(isset($item['operator'])){
					if(!in_array($item['operator'],$operator_list)){
						continue;
					}
				}else{
					$item['operator']="=";
				}
				if(!isset($item['value'])){
					continue;
				}
				if(is_array($item['value'])){
					$array_count=count($item['value']);
					$tmp=implode(",",array_fill(1,$array_count,"?"));					
					$and_where_array[]=" {$item['field']} in ({$tmp})";
					foreach($item['value'] as $k=>$v){
						$bind_data[]=$v;
					}
				}else{				
					$and_where_array[]=" {$item['field']} {$item['operator']} ? ";
					$bind_data[]=$item['value'];
				}	
			}
			
		}
		return $and_where_array;
	}
	
	public static function process_order($array){
		$sql="";
		$order_array=[];
		$order_type_list=["asc","desc"];
		if(isset($array['order'])){			
			foreach($array['order'] as $item){
				if(!in_array($item['field'],static::$field_array)){
					continue;
				}
				if(isset($order_type_list[$item['type']])){
					$order_type=$order_type_list[$item['type']];
					$order_array[]="{$item['field']} {$order_type}";
				}	
			}
			if($order_array){
				$sql="order by ".implode(",",$order_array);
			}
		}
		return $sql;
	}		
	public static function process_limit($array){
		if(isset($array['count'])){
			$count=$array['count']*1;
		}else{
			$count=10;
		}
		
		if(isset($array['page'])){
			$page=$array['page']*1;
		}else{
			$page=0;
		}
	
		$start=$count*$page;
		$sql="limit {$start},{$count}";
		return compact(["page","count","sql"]);
	}
	public static function obtain($array=[]){
		
		$bind_data=[];
		$table_name=static::$table_name;
		$fields=static::process_field($array);
		$and_where=static::process_where($array,$bind_data);		
		$order_str=static::process_order($array);		
		$tmp=static::process_limit($array);
		$limit_str=$tmp['sql'];
		$page=$tmp['page'];
		$count=$tmp['count'];
		
		if(isset($array['custom']['where'])){
			foreach($array['custom']['where'] as $k=>$v){
				$and_where[]=$k;
				$bind_data[]=$v;
			}
		}
		$where_str="";
		if($and_where){
			$where_str="where ".implode(" && ",$and_where);
		}
		$table_str="";
		if(isset($array['custom']['table'])){
			$table_str=$array['custom']['table'];
		}
		
		$group_str="";
		if(isset($array['custom']['group'])){
			$group_array=[];
			foreach($array['custom']['group'] as $f){
				if(!in_array($f,static::$field_array)){
					continue;
				}
				$group_array[]=$f;
			}
			$group_str="group by ".implode(",",$group_array);
		}
		
		if(isset($array['data_status'])){
			$data_status=$array['data_status'];
		}else{
			$data_status=false;
		}
	
		if(!$data_status){
			$start_time=microtime(1);
			if(isset($array['total'])){
				$total=$array['total'];	
			}else{
				$sql1="
					select 
						count(1) total 
					from `{$table_name}` 
					{$where_str} 
				";
				
				$tmp=Container::get("DB")->fetchAssoc($sql1,$bind_data);
				
				if($tmp){
					$total=$tmp['total'];
				}else{
					$total=0;			
				}
			}			
			$total_page=(int)ceil($total/$count);
			$time1=microtime(1)-$start_time;
		}
		
		$list=[];
		if(!$data_status && $total || $data_status){
			$start_time=microtime(1);
			$sql="
				select
					{$fields} 
				from 
					{$table_name}
					{$table_str}
					{$where_str}
					{$group_str}
					{$order_str}
					{$limit_str} 
			";
			$list=Container::get("DB")->fetchAll($sql,$bind_data);
			$time=microtime(1)-$start_time;
		}
	
		if($data_status){
			return $list;		
		}else{
			
			return compact([
				"list",
				
				"page","count",
				
				"total","total_page",
				
				"array","bind_data",
				
				"sql","time",
				
				"sql1","time1",	
			]);
		}
		
	}
	
	public static function auto_add_time($array,$time_name_field="create_time"){
		if(in_array($time_name_field,static::$field_array)){
			$array["{$time_name_field}_int"]=time();
			$array[$time_name_field]=date("Y-m-d H:i:s",$array["{$time_name_field}_int"]);		
		}
		return $array;
	}		
	public static function insert($update){		
		return Container::get("DB")->insert(static::$table_name,$update);	
	}
	public static function update($array){
		return Container::get("DB")->update(static::$table_name,$array['update'],$array['where']);
	}
	public static function delete($where){
		return Container::get("DB")->delete(static::$table_name,$where);
	}
	public static function filterField($data){
		$result=[];
		foreach(static::$field_array as $field){
			if(isset($data[$field])){
				$result[$field]=$data[$field];
			}
		}
		return $result;
	}
	public static function insert_update_delete($array){
		if(isset($array['update'])){
			$update_count=count($array['update']);
			$array['update']=static::filterField($array['update']);
		}else{
			$update_count=0;		
		}
		if(isset($array['where'])){
			$where_count=count($array['where']);
			$array['where']=static::filterField($array['where']);
		}else{
			$where_count=0;
		}
		if($update_count){
			$array['update']=static::auto_add_time($array['update'],"update_time");
			if($where_count){
				$array['update']=static::auto_add_time($array['update']);
			}
		}
		
		$result=[];
		if($update_count && !$where_count){//新增
			$result['type']=0;		
			$result['result']=static::insert($array['update']);			
			$result['last_id']=Container::get("DB")->lastInsertId();		
		}else if($update_count && $where_count){//修改
			$result['type']=1;		
			$result['result']=static::update($array);
			
		}else if(!$update_count && $where_count){//刪除
			$result['type']=2;
			$result['result']=static::delete($array['where']);
		}
		return $result;
	}
}